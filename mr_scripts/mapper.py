#!/usr/bin/env python

import sys
import json
import re

for line in sys.stdin:
    try:
        review = json.loads(line)
        words = re.findall(r'\b\w+\b', review['reviewText'].lower())
        for word in words:
            print(f"{word}\t1")
    except:
        continue