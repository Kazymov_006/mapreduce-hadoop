import sys

out_filename = "data/Movies_and_TV.json"


with open(out_filename, "w") as cop:
    i = 1
    for line in sys.stdin:
        cop.write(line)
        if i == 509260:
            break
        i += 1


hadoop jar /opt/hadoop/hadoop-streaming-3.3.6.jar -files /opt/hadoop/mr_scripts/mapper.py,/opt/hadoop/mr_scripts/reducer.py -mapper "python mapper.py" -reducer "python reducer.py" -input /user/hadoop/map_reduce/input/data.json -output /user/hadoop/map_reduce/output