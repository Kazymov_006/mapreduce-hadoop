#!/usr/bin/env python3

import sys

amogs = {}

for line in sys.stdin:
    try:
        line = line.strip()
        word, count = line.split("\t", 1)
        try:
            count = int(count)
        except ValueError:
            continue
        if word not in amogs:
            amogs[word] = 0
        amogs[word] += count
    except:
        continue

for w, c in amogs.items():
    for i in range(c):
        print(f"{w}\t1")



